Permite consultar el API de [forecast.io](https://developer.forecast.io/) para obtener el clima y hora de las ciudades: Santiago (CL), Zurich (CH), Auckland (NZ), Sydney (AU), Londres (UK), Georgia (USA) .

## Prerequisitos
* Ejecutar el proyecto [rl-api](https://bitbucket.org/jewifcom/rl-api/src/master/)
* [docker](https://docs.docker.com/install/)
* [docker-compose](https://docs.docker.com/compose/install/)

## Ejecución

```
git clone git@bitbucket.org:jewifcom/rl-front.git
cd rl-front
docker-compose up
```
Abrir el navegador en http://localhost