
class SocketHandler {

    availableCities(response, socket) {
        response.forEach(city => {
            socket.emit('getForecast', city);
            console.log(`quering info about city ${city}`);
        });
    }
}

export default SocketHandler;