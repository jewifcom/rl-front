import socketIOClient from 'socket.io-client';

const socket = socketIOClient(`http://${window.location.hostname}:8090`);
export { socket };