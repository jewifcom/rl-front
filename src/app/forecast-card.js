import React, { Component } from 'react';
import moment from 'moment-timezone';
import 'moment/locale/es';

class ForecastCard extends Component {

    render() {
        moment.locale('es');
        const date = moment().tz(this.props.cityInfo.timezone);

        return (
            <div className="row">
                <p className="title">
                    <strong className="capitalize">{this.props.cityInfo.city}</strong>
                </p>
                <p className="description">
                    {date.format('LLLL:s')} - {this.props.cityInfo.temperature}°
                </p>
            </div>
        );
    }
}

export { ForecastCard };