import React, { Component } from 'react';
import { socket } from './app/socket-client';
import { ForecastCard } from './app/forecast-card';
import SocketHandler from './app/socket-handler';
import './App.css';

const REFRESH_TIME = 10000;
const ERROR_MESSAGE = 'How unfortunate! The API Request Failed';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = { cities: [] };
    this.handler = new SocketHandler();
    this.initializeApp();
  }

  initializeApp() {
    socket.emit('getAvailableCities');

    socket.on('availableCities', (response) => {
      this.handler.availableCities(response, socket);
      this.refresh();
    });

    socket.on('updateCityInfo', (cityInfo) => {
      const parsedInfo = JSON.parse(cityInfo);
      this.updateCity(parsedInfo);
    });
  };

  updateCity(cityInfo) {
    console.log(`updating city with ${JSON.stringify(cityInfo)}`);
    if (this.existCityInStateStorage(cityInfo)) {
      this.updateState(cityInfo);
    } else {
      this.addState(cityInfo);
    }
  }

  existCityInStateStorage(cityInfo) {
    const cityIndex = this.state.cities.findIndex((element) => {
      return element.city === cityInfo.city;
    });
    return cityIndex >= 0;
  }

  addState(cityInfo) {
    this.setState(state => {
      const cities = [].concat(state.cities, [cityInfo]);
      return { cities };
    });
  }

  updateState(cityInfo) {
    this.setState(state => {
      const cities = state.cities.map((item, j) => {
        if (item.city === cityInfo.city) {
          return cityInfo;
        }
        return item;
      });
      return { cities };
    });
  }

  refresh() {
    setInterval(() => {
      this.state.cities.forEach(city => {
        try {
          console.log(`querying info about city ${city.city}`);
          this.getForecast(city.city);
        } catch (error) {
          this.handlerError(error, city.city);
        };
      });
    }, REFRESH_TIME);
  }

  getForecast(cityName) {
    if (Math.random() < 0.1) throw new Error(ERROR_MESSAGE);
    socket.emit('getForecast', cityName);
  }

  handlerError(error, cityName) {
    if (error.message === ERROR_MESSAGE) {
      const errorEvent = `Error updating: ${cityName}, with message: <<${error.message}>>`;
      socket.emit('putError', errorEvent);
      try {
        console.log(`retrying ${cityName}`);
        this.getForecast(cityName);
      } catch (error) {
        this.handlerError(error, cityName);
      }
    }
  }

  render() {
    return (
      <div className="App">
        {this.state.cities.map((city) => {
          return <ForecastCard key={city.city} cityInfo={city} />
        })}
      </div>
    );
  }
}

export default App;
